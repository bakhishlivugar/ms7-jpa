package az.ingress.jpa.ms7;

import az.ingress.jpa.ms7.domain.Group;
import az.ingress.jpa.ms7.repository.GroupsRepository;
import az.ingress.jpa.ms7.repository.ProjectRepository;
import az.ingress.jpa.ms7.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.*;

@SpringBootApplication
@RequiredArgsConstructor
public class Ms7Application implements CommandLineRunner {

    private final GroupsRepository groupsRepository;
    private final StudentRepository studentRepository;
    private final ProjectRepository projectRepository;

    private final EntityManagerFactory entityManagerFactory;

    public static void main(String[] args) {
        SpringApplication.run(Ms7Application.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        EntityGraph<?> graph = entityManager.getEntityGraph("graph.Group.students");
        Map<String, Object> hints = new HashMap<>();
        hints.put("javax.persistence.loadgraph", graph);

        Group group = entityManager.find(Group.class, 5L, hints);
        System.out.println(group);
        entityManager.close();

    }
}
