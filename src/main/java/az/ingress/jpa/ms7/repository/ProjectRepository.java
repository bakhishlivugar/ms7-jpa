package az.ingress.jpa.ms7.repository;

import az.ingress.jpa.ms7.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Long> {
}
