package az.ingress.jpa.ms7.repository;

import az.ingress.jpa.ms7.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, Long> {

    Optional<Student> findByName(String name);

    @Query("SELECT DISTINCT name FROM Student WHERE name like (:name)")
    List<String> findWithQuery(String name);

    @Query("SELECT s from Student s where s.age > ?1")
    List<Student> findByAgeQuery(int age);

    @Query("SELECT s from Student s where s.age > :sage and s.name =:name")
    List<Student> findByAgeQuery(@Param("sage") int age, @Param("name") String name);

    @Query(nativeQuery = true, value = "select * from students student0_ where student0_.age>? and student0_.name=?")
    List<Student> findByAgeQuerySQL(@Param("sage") int age, @Param("name") String name);

}
