package az.ingress.jpa.ms7.repository;

import az.ingress.jpa.ms7.domain.Group;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface GroupsRepository extends JpaRepository<Group, Long> {

//    @EntityGraph(
//            value = "Group.students",
//            type = EntityGraph.EntityGraphType.LOAD
//    )
//    Set<Group> getAllGroupsWithStudentsEG();
}
