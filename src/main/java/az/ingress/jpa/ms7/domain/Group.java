package az.ingress.jpa.ms7.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@Table(name = Group.TABLE_NAME)
@AllArgsConstructor
@NoArgsConstructor
@NamedEntityGraph(name = "graph.Group.students",
                    attributeNodes = @NamedAttributeNode(value = "students", subgraph = "students"),
                    subgraphs = @NamedSubgraph(name = "students", attributeNodes = @NamedAttributeNode("projects")))
public class Group {
    public static final String TABLE_NAME = "student_groups";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    @ManyToMany(mappedBy = "groups", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    Set<Student> students;

}
