package az.ingress.jpa.ms7.domain;

import lombok.*;

import javax.persistence.*;

import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "student")
@EqualsAndHashCode(exclude = "student")
@Table(name = Project.TABLE_NAME)
public class Project {
    public static final String TABLE_NAME = "student_projects";

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    private String name;

    private String description;

    @ManyToOne
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    private Student student;

}
